# Trig

`trig` is a package that provides a variety of common and lesser-known
trigonometric functions.

The included functions are:

- sine (`sin`)
- cosine (`cos`)
- tangent (`tan`)
- secant (`sec`)
- cosecant (`csc`)
- cotangent (`cot`)
- versine (`ver`)
- coversine (`cvs`)
- vercosine (`vcs`)
- covercosine (`cvc`)
- haversine (`hvs`)
- hacoversine (`hcv`)
- havercosine (`hvc`)
- hacovercosine (`hcc`)
- exsecant (`exs`)
- excosecant (`exc`)
- chord (`crd`)

each of which also has a hyperbolic (suffixed with `h`), inverse (prefixed
with `a`) and inverse hyperbolic variant.
